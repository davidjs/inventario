inventario = {}
def listar():
    ...
    for codigo, valores in inventario.items():
        nombre = valores['nombre']
        precio = valores['precio']
        cantidad = valores['cantidad']
        print(f'{codigo}: {nombre}, precio: {precio}, cantidad: {cantidad}')


def consultar(codigo: str):
    ...
    if codigo in inventario:
        valores = inventario[codigo]
        nombre = valores['nombre']
        precio = valores['precio']
        cantidad = valores['cantidad']
        print(f'{codigo}: {nombre}, precio: {precio}, cantidad: {cantidad}')


def agotados():
    ...
    for codigo, cantidad in inventario.items():
        if cantidad['cantidad'] == 0:
            print(f'El articulo {codigo} se ha agotado')


def pide_articulo() -> (str, str, float, int):
    ...
    codigo = str(input('Código de artículo: '))
    nombre = str(input('Nombre: '))
    precio = float(input('Precio: '))
    cantidad = int(input('Cantidad: '))
    return codigo, nombre, precio, cantidad



    if opcion == '0':
            seguir = False

    elif opcion == '1':
            ...
            codigo, nombre, precio, cantidad = pide_articulo()
            insertar(codigo, nombre, precio, cantidad)
    elif opcion == '2':
            listar()
    elif opcion == '3':
            idConsulta = str(input('Código del artículo a consultar: '))
            consultar(idConsulta)
    elif opcion == '4':
            agotados()
    else:
            print("Opción incorrecta")
